package pe.tss.tiny1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tiny1Application {

	public static void main(String[] args) {
		SpringApplication.run(Tiny1Application.class, args);
	}

}
